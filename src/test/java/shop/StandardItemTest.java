package shop;

import cz.cvut.eshop.shop.Item;
import cz.cvut.eshop.shop.StandardItem;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import static org.junit.Assert.*;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class StandardItemTest {


    //Use this block for forced error
    //static StandardItem item = new StandardItem(0, "1", 2, "3", 4);

    //Use this for right functionality
    StandardItem item;
    @Before
    public void init(){
         item = new StandardItem(0, "1", 2, "3", 4);
    }


    @Test
    public void testDeepCopy() {
        StandardItem copyItem = item.copy();
        assertEquals(item, copyItem);
    }


    @Test
    public void _testSetLoyaltyPoints() {
        item.setLoyaltyPoints(300);
        assertEquals(300,item.getLoyaltyPoints());
    }



    @Test
    public void testGetLoyaltyPoints()
    {
        assertEquals(4,item.getLoyaltyPoints());
    }




    @Test
    public void testItemEquals() {

        StandardItem a = new StandardItem(0, "1", 2, "3", 4);
        assertEquals(a, item);

    }
}
